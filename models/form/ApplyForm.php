<?php

namespace app\models\form;

use Yii;
use yii\base\Model;
use app\models\Candidate;

/**
 * Nomzodlardan ariza qabul qilish formasi modeli.
 */
class ApplyForm extends Model
{
    public $name;
    public $family_name;
    public $address;
    public $country_of_origin;
    public $email_address;
    public $phone_number;
    public $age;
    public $verifyCode;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // to'ldirish shart bo'lgan maydonlar
            [
                ['name', 'family_name', 'address', 'country_of_origin', 'email_address', 'phone_number', 'age'],
                'required',
                'message' => Yii::t('app', "To'ldirish shart")
            ],
            // familya va isma kamida 5 ta belgidan iborat bo'lishi kerak
            [
                ['name', 'family_name'],
                'string',
                'min' => 5,
                'message' => Yii::t('app', "Kamida 5 ta belgi bo'lishi kerak")
            ],
            // Manzil kamida 10 ta belgi bo'lishi kerak
            [
                ['address'],
                'string',
                'min' => 10,
                'message' => Yii::t('app', "Kamida 10 ta belgi bo'lishi kerak")
            ],
            // bu maydonlar varchar bo'lgani uchun maksimum 255
            [
                ['name', 'family_name', 'address', 'country_of_origin', 'email_address'],
                'string',
                'max' => 255
            ],
            // telefon raqam +998999999999 formatda bo'lishi kerak
            [
                ['phone_number'],
                'match',
                'pattern' => '/^\+\d{12}$/i',
                'message' => Yii::t('app', "Telefon formati noto'g'ri")
            ],
            [
                'email_address',
                'email',
            ],
            // yosh raqam bo'lishi kerak
            [
                ['age'],
                'integer',
                'message' => Yii::t('app', "Son bo'lishi kerak")
            ],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * Nomzodni registratsiya qilish
     * @return bool
     */
    public function register()
    {
        if ($this->validate()) {
            $candidate = new Candidate();
            $candidate->attributes = $this->attributes;

            return $candidate->save();
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Ism'),
            'family_name' => Yii::t('app', 'Familiya'),
            'address' => Yii::t('app', 'Manzil'),
            'country_of_origin' => Yii::t('app', 'Davlat'),
            'email_address' => Yii::t('app', 'Email'),
            'phone_number' => Yii::t('app', 'Telefon'),
            'age' => Yii::t('app', 'Yosh'),
            'verifyCode' => Yii::t('app', 'Tasdiqlash kodi'),
        ];
    }
}
