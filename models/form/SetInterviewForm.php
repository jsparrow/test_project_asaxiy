<?php


namespace app\models\form;

use Yii;
use yii\base\Model;
use app\models\Candidate;

class SetInterviewForm extends Model
{
    public $interview_time;
    public $note;

    public function rules()
    {
        return [
            [['interview_time', 'note'], 'required', 'message' => Yii::t('app', "To'ldirish shart")],
            ['interview_time', 'datetime', 'format' => 'php: d-m-Y H:i'],
            ['note', 'trim'],
            ['note', 'string', 'min' => '1'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'interview_time' => Yii::t('app', "Intervyu vaqti"),
            'note' => Yii::t('app', "Eslatma"),
        ];
    }

    /**
     * Intervyu ni belgilash (saqlash)
     *
     * @param Candidate $candidate
     * @return bool
     */
    public function setInterview(Candidate $candidate):bool
    {
        if (!$this->validate()) {
            return false;
        }

        $candidate->interview_time = $this->getInterviewTime();
        $candidate->note = $this->note;
        $candidate->status = Candidate::STATUS_INTERVIEW_SCHEDULED;

        return $candidate->save();
    }

    public function getInterviewTime() {
        return date('Y-m-d H:i:s', strtotime($this->interview_time));
    }

    public function setInterviewTime($interviewTime) {
        $this->interview_time = date('d-m-Y H:i', strtotime($interviewTime));
    }
}