<?php


namespace app\models\form;

use Yii;
use yii\base\Model;
use app\models\Candidate;

class ChangeStatusForm extends Model
{
    public $status;

    public function rules()
    {
        return [
            [['status'], 'required', 'message' => Yii::t('app', "To'ldirish shart")],
            [
                'status',
                'in',
                'range' => [
                    Candidate::STATUS_NEW,
                    Candidate::STATUS_INTERVIEW_SCHEDULED,
                    Candidate::STATUS_ACCEPTED,
                    Candidate::STATUS_NOT_ACCEPTED,
                ],
                'message' => Yii::t('app', "Status qiymati noto'g'ri")
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'status' => Yii::t('app', "Status"),
        ];
    }

    /**
     * Statusni o'zgartirish
     *
     * @param Candidate $candidate
     * @return bool
     */
    public function changeStatus(Candidate $candidate):bool
    {
        if (!$this->validate()) {
            return false;
        }

        $candidate->status = $this->status;

        if ($this->status == Candidate::STATUS_ACCEPTED) {
            $candidate->hired = true;
        }

        return $candidate->save();
    }
}