<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "candidate".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $family_name
 * @property string|null $address
 * @property string|null $country_of_origin
 * @property string|null $email_address
 * @property string|null $phone_number
 * @property int|null $age
 * @property int|null $hired
 * @property int $status
 * @property string $interview_time
 * @property string $note
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property-read string $statusHtmlLabel
 * @property-read string|null $createdDate
 * @property-read string|null $updatedDate
 * @property-read string $hiredLabel
 */
class Candidate extends \yii\db\ActiveRecord
{
    const STATUS_NEW                 = 1; // yangi
    const STATUS_INTERVIEW_SCHEDULED = 2; // intervyu belgilangan
    const STATUS_ACCEPTED            = 3; // qabul qilingan
    const STATUS_NOT_ACCEPTED        = 4; // qabul qilinmagan

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'candidate';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['age', 'hired', 'created_at', 'updated_at', 'status'], 'integer'],
            ['interview_time', 'datetime', 'format' => 'php: Y-m-d H:i:s'],
            [['name', 'family_name', 'address', 'country_of_origin', 'email_address'], 'string', 'max' => 255],
            ['note', 'string'],
            [['phone_number'], 'string', 'max' => 13],
            ['status', 'default', 'value' => self::STATUS_NEW],
            ['status', 'in', 'range' => [
                    self::STATUS_NEW,
                    self::STATUS_INTERVIEW_SCHEDULED,
                    self::STATUS_ACCEPTED,
                    self::STATUS_NOT_ACCEPTED,
                ]
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Ism'),
            'family_name' => Yii::t('app', 'Familiya'),
            'address' => Yii::t('app', 'Manzil'),
            'country_of_origin' => Yii::t('app', 'Davlat'),
            'email_address' => Yii::t('app', 'E-mail'),
            'phone_number' => Yii::t('app', 'Telefon'),
            'age' => Yii::t('app', 'Yoshi'),
            'hired' => Yii::t('app', 'Ishga qabul qilingan'),
            'created_at' => Yii::t('app', 'Yaratilgan vaqti'),
            'updated_at' => Yii::t('app', "O'zgartirilgan vaqti"),
            'updatedDate' => Yii::t('app', "O'zgartirilgan vaqti"),
            'interview_time' => Yii::t('app', "Intervyu vaqti"),
            'note' => Yii::t('app', "Eslatma"),
            'createdDate' => Yii::t('app', 'Yaratilgan sanasi'),
            'statusHtmlLabel' => Yii::t('app', 'Status'),
            'hiredLabel' => Yii::t('app', "Ishga qabul qilingan"),
        ];
    }

    /**
     * Yaratilgan sanasini qaytaradi (formati: 17.02.2020 03:49:55)
     *
     * @return string|null
     */
    public function getCreatedDate():?string
    {
        if (empty($this->created_at))
        {
            return null;
        }

        return date('d.m.Y H:i:s', $this->created_at);
    }

    /**
     * Yangilangan sanasini qaytaradi (formati: 17.02.2020 03:49:55)
     *
     * @return string|null
     */
    public function getUpdatedDate():?string
    {
        if (empty($this->updated_at))
        {
            return null;
        }

        return date('d.m.Y H:i:s', $this->updated_at);
    }


    /**
     * Status raqamiga qarab matn tarzida qaytaradi
     *
     * @return string
     */
    public function getStatusLabel():string
    {
        $statusLabel = '';
        switch ($this->status) {
            case self::STATUS_NEW:
                $statusLabel = Yii::t('app', 'Yangi');
                break;
            case self::STATUS_INTERVIEW_SCHEDULED:
                $statusLabel = Yii::t('app', 'Intervyu belgilangan');
                break;
            case self::STATUS_ACCEPTED:
                $statusLabel = Yii::t('app', 'Qabul qilingan');
                break;
            case self::STATUS_NOT_ACCEPTED:
                $statusLabel = Yii::t('app', 'Qabul qilinmagan');
                break;
        }

        return $statusLabel;
    }


    /**
     * Status raqamiga qarab html (bootstrap 4 badge) tarzida qaytaradi
     *
     * @return string
     */
    public function getStatusHtmlLabel():string
    {
        $badgeType = '';
        switch ($this->status) {
            case self::STATUS_NEW:
                $badgeType = 'warning';
                break;
            case self::STATUS_INTERVIEW_SCHEDULED:
                $badgeType = 'primary';
                break;
            case self::STATUS_ACCEPTED:
                $badgeType = 'success';
                break;
            case self::STATUS_NOT_ACCEPTED:
                $badgeType = 'danger';
                break;
        }

        return "<span class='badge badge-pill badge-{$badgeType}'>" . $this->getStatusLabel() . '</span>';
    }

    /**
     * Ishqa qabul qilinganligi haqida matn qaytaradi {"Ha", "Yo'q"}
     *
     * @return string
     */
    public function getHiredLabel():string
    {
        return $this->hired == 1 ? Yii::t('app', "Ha") : Yii::t('app', "Yo'q");
    }

    /**
     * Statuslar ro'yhatini key => value shaklida qayataradi
     *
     * @return array
     */
    public static function getStatusList():array
    {
        return [
            self::STATUS_NEW => Yii::t('app', 'Yangi'),
            self::STATUS_INTERVIEW_SCHEDULED => Yii::t('app', 'Intervyu belgilangan'),
            self::STATUS_ACCEPTED => Yii::t('app', 'Qabul qilingan'),
            self::STATUS_NOT_ACCEPTED => Yii::t('app', 'Qabul qilinmagan')  ,
        ];
    }
}
