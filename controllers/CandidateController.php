<?php

namespace app\controllers;

use app\models\form\ChangeStatusForm;
use app\models\form\SetInterviewForm;
use Yii;
use app\models\Candidate;
use app\models\search\CandidateSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CandidateController implements the CRUD actions for Candidate model.
 */
class CandidateController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],

            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Candidate models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CandidateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Candidate model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Candidate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Candidate();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Candidate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Candidate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('info', Yii::t('app', "Nomzod o'chirildi!"));

        return $this->redirect(['index']);
    }

    /**
     * Intervyuni belgilash uchun
     *
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionSetInterview($id)
    {
        $candidate = $this->findModel($id);
        $setInterviewForm = new SetInterviewForm();
        $setInterviewForm->note = $candidate->note;
        $setInterviewForm->setInterviewTime($candidate->interview_time);

        if ($setInterviewForm->load(Yii::$app->request->post()))
        {
            if ($setInterviewForm->setInterview($candidate)) {
                Yii::$app->session->setFlash('success', Yii::t('app', "Intervyu muvaffaqiyatli belgilandi"));

                return $this->redirect(['candidate/view', 'id' => $candidate->id]);
            }

            Yii::$app->session->setFlash('error', Yii::t('app', "Validatsiyada xatolik yuz berdi"));

        }

        return $this->render('set_interview', [
            'setInterviewForm' => $setInterviewForm,
        ]);
    }

    /**
     * Intervyuni belgilash uchun
     *
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionChangeStatus($id)
    {
        $candidate = $this->findModel($id);
        $changeStatusForm = new ChangeStatusForm();
        $changeStatusForm->status = $candidate->status;

        if ($changeStatusForm->load(Yii::$app->request->post()))
        {
            if ($changeStatusForm->changeStatus($candidate)) {
                Yii::$app->session->setFlash('success', Yii::t('app', "Status muvaffaqiyatli o'zgartirildi"));

                return $this->redirect(['candidate/view', 'id' => $candidate->id]);
            }

            Yii::$app->session->setFlash('error', Yii::t('app', "Validatsiyada xatolik yuz berdi"));

        }

        return $this->render('change_status', [
            'changeStatusForm' => $changeStatusForm,
        ]);
    }

    /**
     * Finds the Candidate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Candidate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Candidate::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
