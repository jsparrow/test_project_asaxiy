<?php

use yii\db\Migration;

/**
 * Class m210216_204352_crate_candidate_table
 */
class m210216_204352_crate_candidate_table extends Migration
{

    public function up()
    {
        $this->createTable('candidate', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'family_name' => $this->string(),
            'address' => $this->string(),
            'country_of_origin' => $this->string(),
            'email_address' => $this->string(),
            'phone_number' => $this->char(13),
            'age' => $this->tinyInteger()->unsigned(),
            'hired' => $this->boolean(),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ]);
    }

    public function down()
    {
        $this->dropTable('candidate');
    }

}
