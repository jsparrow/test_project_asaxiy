<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%candidate}}`.
 */
class m210216_212447_add_interview_time_column_to_candidate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%candidate}}', 'interview_time', $this->datetime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%candidate}}', 'interview_time');
    }
}
