<?php

use yii\db\Migration;

/**
 * Class m210216_194525_add_admin_user_row_to_users
 */
class m210216_194525_add_admin_user_row_to_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        // admin foydalanuvchini qo'shish
        $this->insert('user', [
            'email' => 'admin@example.com',
            'auth_key' => Yii::$app->getSecurity()->generateRandomString(32),
            'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('admin'),
            'status' => 10,
            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        // admin foydalanuvchini o'chirish
        $this->delete('user', [
            'email' => 'admin@example.com',
        ]);
    }

}
