<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%candidate}}`.
 */
class m210216_212552_add_note_column_to_candidate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%candidate}}', 'note', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%candidate}}', 'note');
    }
}
