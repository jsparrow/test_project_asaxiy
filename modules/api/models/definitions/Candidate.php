<?php

namespace app\modules\api\models\definitions;

/**
 * @SWG\Definition(required={"name", "family_name", "address"})
 *
 * @SWG\Property(property="id", type="integer")
 * @SWG\Property(property="name", type="string")
 * @SWG\Property(property="family_name", type="string")
 * @SWG\Property(property="address", type="string")
 * @SWG\Property(property="country_of_origin", type="string")
 * @SWG\Property(property="email_address", type="string")
 * @SWG\Property(property="phone_number", type="string")
 * @SWG\Property(property="age", type="integer")
 * @SWG\Property(property="hired", type="boolean")
 */
class Candidate
{

}