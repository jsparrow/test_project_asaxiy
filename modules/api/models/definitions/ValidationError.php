<?php

namespace app\modules\api\models\definitions;

/**
 * @SWG\Definition(
 *     type="object",
 *     required={"field", "message"}
 *     )
 *
 * @SWG\Property(property="field", type="string")
 * @SWG\Property(property="message", type="string")
 */
class ValidationError
{

}