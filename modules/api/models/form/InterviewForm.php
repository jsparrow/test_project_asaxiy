<?php


namespace app\modules\api\models\form;

use app\models\Candidate;
use app\modules\api\models\CandidateResource;
use Yii;
use yii\base\Model;
use yii\web\NotFoundHttpException;

class InterviewForm extends Model
{
    public $interview_time;
    public $note;

    public function rules()
    {
        return [
            [['interview_time', 'note'], 'required', 'message' => Yii::t('app', "To'ldirish shart")],
            ['interview_time', 'datetime', 'format' => 'php: Y-m-d H:i:s'],
            ['note', 'trim'],
            ['note', 'string', 'min' => '1'],
        ];
    }

    /**
     * Nomzod uchun intevyu vaqti va eslatmani yozib saqlash
     *
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function setInterviewById($id)
    {
        $candidate = CandidateResource::findOne(['id' => $id]);

        if ($candidate === null) {
            throw new NotFoundHttpException(Yii::t('app', "{$id} identifikatorli ariza mavjud emas"));
        }

        $candidate->setScenario(CandidateResource::SCENARIO_SET_INTERVIEW);
        $candidate->attributes = $this->attributes;
        $candidate->status = CandidateResource::STATUS_INTERVIEW_SCHEDULED;

        $success = $candidate->save();
        if ($success) {
            return $candidate;
        }

        return $candidate->getFirstErrors();
    }
}