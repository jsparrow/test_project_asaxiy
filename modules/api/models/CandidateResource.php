<?php


namespace app\modules\api\models;

use Yii;
use app\models\Candidate;

class CandidateResource extends Candidate
{
    const SCENARIO_SET_INTERVIEW = 'set-interview';

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // to'ldirish shart bo'lgan maydonlar (default scenario uchun)
            'required' => [
                ['name', 'family_name', 'address', 'country_of_origin', 'email_address', 'phone_number', 'age'],
                'required',
                'message' => Yii::t('app', "To'ldirish shart"),
                'on' => [self::SCENARIO_DEFAULT]
            ],
            // to'ldirish shart bo'lgan maydonlar (default scenario uchun)
            [
                ['name', 'family_name', 'address', 'country_of_origin', 'email_address', 'phone_number', 'age'],
                'required',
                'message' => Yii::t('app', "To'ldirish shart"),
                'on' => [self::SCENARIO_SET_INTERVIEW]
            ],
            // familya va isma kamida 5 ta belgidan iborat bo'lishi kerak
            [
                ['name', 'family_name'],
                'string',
                'min' => 5,
                'message' => Yii::t('app', "Kamida 5 ta belgi bo'lishi kerak"),
            ],
            // Manzil kamida 10 ta belgi bo'lishi kerak
            [
                ['address'],
                'string',
                'min' => 10,
                'message' => Yii::t('app', "Kamida 10 ta belgi bo'lishi kerak")
            ],
            // bu maydonlar varchar bo'lgani uchun maksimum 255
            [
                ['name', 'family_name', 'address', 'country_of_origin', 'email_address'],
                'string',
                'max' => 255
            ],
            // telefon raqam +998999999999 formatda bo'lishi kerak
            [
                ['phone_number'],
                'match',
                'pattern' => '/^\+\d{12}$/i',
                'message' => Yii::t('app', "Telefon formati noto'g'ri")
            ],
            [
                'email_address',
                'email',
            ],
            // yoshi raqam bo'lishi kerak
            [
                ['age'],
                'integer',
                'message' => Yii::t('app', "Son bo'lishi kerak")
            ],
            ['interview_time', 'datetime', 'format' => 'php: Y-m-d H:i:s', 'on' => [self::SCENARIO_SET_INTERVIEW]],
            ['note', 'string', 'on' => [self::SCENARIO_SET_INTERVIEW]],
        ];
    }

    public function fields()
    {
        $fields = parent::fields();

        unset($fields['created_at'], $fields['updated_at'], $fields['status']);

        $fields['hired'] = function () {
            return $this->hired == 1;
        };

        return $fields;
    }
}