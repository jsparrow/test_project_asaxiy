<?php


namespace app\modules\api\controllers;

use app\modules\api\models\form\InterviewForm;
use Yii;
use yii\helpers\Url;
use yii\rest\ActiveController;
use app\modules\api\models\CandidateResource;


/**
 * Class CandidateController
 */
class CandidateController extends ActiveController
{
    public $modelClass = CandidateResource::class;
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    /**
     * @SWG\Post(path="/candidates/interview/{id}",
     *     tags={"Candidate"},
     *     summary="Intervyu ni belgilash",
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          type="integer"
     *      ),
     *     @SWG\Parameter(
     *         required=true,
     *         in="body",
     *         name="body",
     *         type="json",
     *         @SWG\Schema(
     *              @SWG\Property( property="interview_time", type="string", example="2021-02-17 17:00:00"),
     *              @SWG\Property( property="note", type="string", example="Omad...")
     *         )
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "Interview muvaffaqiyatli belgilandi",
     *         @SWG\Schema(ref="#definitions/Candidate")
     *     ),
     *     @SWG\Response(
     *         response = 422,
     *         description = "Validatsiyada xatolik",
     *         @SWG\Schema(
     *              @SWG\Items(ref="#definitions/ValidationError")
     *         )
     *     )
     * )
     *
     * Intervyuni belgilash uchun action
     *
     * @param int $id
     */
    public function actionInterview($id)
    {
        $request = Yii::$app->getRequest();
        $response = Yii::$app->getResponse();

        $interviewForm = new InterviewForm();
        $interviewForm->load($request->getBodyParams(), '');

        $result = $interviewForm->setInterviewById($id);

        // agar validatsiyadan o'tmasa 422 qaytaramiz
        if (is_array($result)) {
            $response->setStatusCode(422);
        }

        return $result;
    }


    /**
     * @SWG\Get(path="/candidates/{id}",
     *     tags={"Candidate"},
     *     summary="Nomzod arizasini identifikatori bo'yicha qayatardi",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          type="integer"
     *      ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "Candidate object response",
     *         @SWG\Schema(ref="#definitions/Candidate")
     *     )
     * )
     */
    public function getCandidatesById() {}

    /**
     * @SWG\Get(path="/candidates",
     *     tags={"Candidate"},
     *     summary="Nomzodlar ro'yhati kolleksiyasini qayataradi.",
     *     @SWG\Response(
     *         response = 200,
     *         description = "Candidate collection response",
     *         @SWG\Schema(
     *            @SWG\Property(
     *              property="items",
     *              type="array",
     *              @SWG\Items(ref="#definitions/Candidate")
     *            )
     *         )
     *     )
     * )
     */
    public function getCandidates() {}


    /**
     * @SWG\Post(path="/candidates",
     *     tags={"Candidate"},
     *     summary="Ariza yaratish",
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *         required=true,
     *         in="body",
     *         name="body",
     *         type="json",
     *         @SWG\Schema(
     *              @SWG\Property( property="name", ref="#/definitions/Candidate/properties/name" ),
     *              @SWG\Property( property="family_name", ref="#/definitions/Candidate/properties/family_name" ),
     *              @SWG\Property( property="address", ref="#/definitions/Candidate/properties/address" ),
     *              @SWG\Property( property="country_of_origin", ref="#/definitions/Candidate/properties/country_of_origin" ),
     *              @SWG\Property( property="email_address", ref="#/definitions/Candidate/properties/email_address" ),
     *              @SWG\Property( property="phone_number", ref="#/definitions/Candidate/properties/phone_number" ),
     *              @SWG\Property( property="age", ref="#/definitions/Candidate/properties/age" )
     *         )
     *     ),
     *     @SWG\Response(
     *         response = 201,
     *         description = "Ariza muvaffaqiyatli yaratildi",
     *         @SWG\Schema(ref="#definitions/Candidate")
     *     ),
     *     @SWG\Response(
     *         response = 422,
     *         description = "Validatsiyada xatolik",
     *         @SWG\Schema(
     *              @SWG\Items(ref="#definitions/ValidationError")
     *         )
     *     )
     * )
     */
    public function postCandidates() {}

    /**
     * @SWG\Put(path="/candidates/{id}",
     *     tags={"Candidate"},
     *     summary="Arizani yangilash",
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          type="integer"
     *      ),
     *     @SWG\Parameter(
     *         required=true,
     *         in="body",
     *         name="body",
     *         type="json",
     *         @SWG\Schema(
     *              @SWG\Property( property="name", ref="#/definitions/Candidate/properties/name" ),
     *              @SWG\Property( property="family_name", ref="#/definitions/Candidate/properties/family_name" ),
     *              @SWG\Property( property="address", ref="#/definitions/Candidate/properties/address" ),
     *              @SWG\Property( property="country_of_origin", ref="#/definitions/Candidate/properties/country_of_origin" ),
     *              @SWG\Property( property="email_address", ref="#/definitions/Candidate/properties/email_address" ),
     *              @SWG\Property( property="phone_number", ref="#/definitions/Candidate/properties/phone_number" ),
     *              @SWG\Property( property="age", ref="#/definitions/Candidate/properties/age" )
     *         )
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "Ariza muvaffaqiyatli yangilandi",
     *         @SWG\Schema(ref="#definitions/Candidate")
     *     ),
     *     @SWG\Response(
     *         response = 422,
     *         description = "Validatsiyada xatolik",
     *         @SWG\Schema(
     *              @SWG\Items(ref="#definitions/ValidationError")
     *         )
     *     )
     * )
     */
    public function putCandidates() {}


    /**
     * @SWG\Delete(path="/candidates/{id}",
     *     tags={"Candidate"},
     *     summary="Nomzod arizasini berilgan id bo'yicha o'chiradi",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          type="integer"
     *      ),
     *     @SWG\Response(
     *         response = 204,
     *         description = "Ariza muvaffaqiyatli o'chirildi"
     *     )
     * )
     */
    public function deleteCandidates() {}
}