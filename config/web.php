<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'name' => 'HR',
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'defaultRoute' => 'site/apply',
    'timeZone' => 'Asia/Tashkent',
    'language' => 'uz',
    'sourceLanguage' => 'uz',
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'modules' => [
        'api' => [
            'class' => 'app\modules\api\Module',
        ],
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'gKVm7QWymbxQ4azx-IBPmfRSD1f75_qe',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                '/'                 => 'site/apply',
                'apply'             => 'site/apply',
                'login'             => 'site/login',
                'logout'            => 'site/logout',
                'site/captcha'      => 'site/captcha',
                'api/documentation' => 'site/docs',
                '/site/json-schema' => 'site/json-schema',

                // nomzod uchun crud rules
                'candidate'                        => 'candidate/index',
                'candidate/create'                 => 'candidate/create',
                'candidate/view/<id:\d+>'          => 'candidate/view',
                'candidate/delete/<id:\d+>'        => 'candidate/delete',
                'candidate/set-interview/<id:\d+>' => 'candidate/set-interview',
                'candidate/change-status/<id:\d+>' => 'candidate/change-status',

                // api
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => 'api/candidate',
                    'extraPatterns' => [
                        'POST interview/<id:\d+>' => 'interview',
                    ],
                ]
            ],
        ],

    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
