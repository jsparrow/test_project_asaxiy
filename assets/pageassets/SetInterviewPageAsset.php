<?php


namespace app\assets\pageassets;


use app\assets\AppAsset;
use app\assets\DateTimePickerAsset;
use yii\web\AssetBundle;

class SetInterviewPageAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/pages/set-interview-page.js',
    ];

    public $depends = [
        AppAsset::class,
        DateTimePickerAsset::class,
    ];
}