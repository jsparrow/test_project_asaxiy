<?php


namespace app\assets;


use yii\web\AssetBundle;

class DateTimePickerAsset extends AssetBundle
{
    public $baseUrl = '@web';
    public $basePath = '@webroot';

    public $css = [
        'plugins/datetimepicker/jquery.datetimepicker.min.css',
    ];

    public $js = [
        'plugins/datetimepicker/jquery.datetimepicker.full.min.js',
    ];
}