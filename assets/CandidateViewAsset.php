<?php


namespace app\assets;


use yii\web\AssetBundle;

class CandidateViewAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/'
    ];
}