<?php

use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $applyForm \app\models\form\ApplyForm */
/* @var $form yii\widgets\ActiveForm */


$this->title = Yii::t('app', 'Ariza topshirish');
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
    <div class="site-apply col-5">
        <div class="card shadow">
            <div class="card-header">
                <h2 class="text-center"><?= Html::encode($this->title) ?></h2>
            </div>
            <div class="card-body">

                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($applyForm, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($applyForm, 'family_name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($applyForm, 'address')->textInput(['maxlength' => true]) ?>

                <?= $form->field($applyForm, 'country_of_origin')->textInput(['maxlength' => true]) ?>

                <?= $form->field($applyForm, 'email_address')->textInput(['maxlength' => true]) ?>

                <?= $form->field($applyForm, 'phone_number')->textInput(['maxlength' => true]) ?>

                <?= $form->field($applyForm, 'age')->input('number', ['step' => 1, 'min' => 1]) ?>

                <?= $form->field($applyForm, 'verifyCode')->widget(Captcha::class, [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ]) ?>

                <div class="form-group">
                    <div class="col-lg-offset-1 col-lg-11">
                        <?= Html::submitButton(Yii::t('app', "Jo'natish"), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>

    </div>
</div>
