<?php

use app\models\Candidate;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $changeStatusForm \app\models\form\SetInterviewForm */
/* @var $form yii\widgets\ActiveForm */


$this->title = Yii::t('app', "Statusni o'zgartirish");
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Nomzodlar'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
    <div class="site-apply col-5 justify-content-center">
        <div class="card shadow">
            <div class="card-header">
                <h2 class="text-center"><?= Html::encode($this->title) ?></h2>
            </div>
            <div class="card-body">

                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($changeStatusForm, 'status')->dropDownList(Candidate::getStatusList()) ?>

                <div class="form-group">
                    <div class="col-lg-offset-1 col-lg-11">
                        <?= Html::submitButton(Yii::t('app', "Saqlash"), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>

    </div>
</div>
