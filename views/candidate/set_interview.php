<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use app\assets\pageassets\SetInterviewPageAsset;

/* @var $this yii\web\View */
/* @var $setInterviewForm \app\models\form\SetInterviewForm */
/* @var $form yii\widgets\ActiveForm */

SetInterviewPageAsset::register($this);

$this->title = Yii::t('app', 'Intervyu belgilash');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Nomzodlar'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
    <div class="site-apply col-5 justify-content-center">
        <div class="card shadow">
            <div class="card-header">
                <h2 class="text-center"><?= Html::encode($this->title) ?></h2>
            </div>
            <div class="card-body">

                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($setInterviewForm, 'interview_time')->textInput([
                    'data' => [
                        'date-time-picker' => '1'
                    ]
                ]) ?>

                <?= $form->field($setInterviewForm, 'note')->textarea(['rows' => 4]) ?>

                <div class="form-group">
                    <div class="col-lg-offset-1 col-lg-11">
                        <?= Html::submitButton(Yii::t('app', "Saqlash"), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>

    </div>
</div>
