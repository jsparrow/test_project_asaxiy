<?php

use app\models\Candidate;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CandidateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Nomzodlar');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="candidate-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-hover table-bordered shadow'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'family_name',
            'createdDate',
            [
                'attribute' => 'status',
                'value' => function(Candidate $model) {
                    return $model->statusHtmlLabel;
                },
                'filter' => Candidate::getStatusList(),
                'format' => 'raw',
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete} {set-interview} {change-status}',
                'buttons' => [
                    'update' =>  function($url,$model) {
                        return Html::a('<i class="fas fa-edit"></i>', $url, [
                            'title' => Yii::t('app', 'update')
                        ]);
                    },
                    'view' =>  function($url,$model) {
                        return Html::a('<i class="fas fa-eye"></i>', $url, [
                            'title' => Yii::t('app', 'view')
                        ]);
                    },
                    'delete' => function($url,$model) {
                        return Html::a('<i class="fas fa-trash"></i>', $url, [
                            'title' => Yii::t('app', 'delete'),
                            'data' => [
                                'method' => 'post',
                                'confirm' => Yii::t('app', "Siz rostdan ham shu nomzodni o'chirishni hohlaysizmi?")
                            ]
                        ]);
                    },
                    'set-interview' => function($url, Candidate $model) {
                        return Html::a('<i class="far fa-clock"></i>', $url, [
                            'title' => Yii::t('app', 'Intervyu belgilash'),
                            'data' => [
                                'id' => $model->id
                            ]
                        ]);
                    },
                    'change-status' => function($url, Candidate $model) {
                        return Html::a('<i class="fas fa-exchange-alt"></i>', $url, [
                            'title' => Yii::t('app', "Statusni o'zgartirish"),
                            'data' => [
                                'id' => $model->id
                            ]
                        ]);
                    }
                ]
            ],
        ],
    ]); ?>

</div>
