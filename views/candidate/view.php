<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Candidate */

$this->title = $model->name . ' ' . $model->family_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Nomzodlar'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="candidate-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', "Siz rostdan ham shu nomzodni o'chirishni istaysizmi?"),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'family_name',
            'address',
            'country_of_origin',
            'email_address:email',
            'phone_number',
            'age',
            'hiredLabel',
            'statusHtmlLabel:html',
            'createdDate',
            'updatedDate',
            [
                'attribute' => 'interview_time',
                'value' => function(\app\models\Candidate $model) {
                    return date('d.m.Y H:i:s', strtotime($model->interview_time));
                }
            ],
            'note:ntext',
        ],
    ]) ?>

</div>
